# Wallflower UI

Frontend UI (react) for controlling Nanoleaf Light Panels over the internet.

[![Netlify Status](https://api.netlify.com/api/v1/badges/c7dcdf93-8ec0-47fa-a89f-a5cae768ca60/deploy-status)](https://app.netlify.com/sites/wallflower/deploys)


## Development

 * `netlify dev` to start development server

## Deployment

 * Automatically deployed using netlify

## Notes
    * nanoleaves source was copied to src/ and modifed
        * Can't recall original modifications
        * Upated to support json `/panelLayout/layout` response (previously was space separated string)
    * Although I contributed back some [SVG fixes](https://github.com/cbartram/nanoleaf-layout/pull/31), I am using a fork (layout.js) of [nanoleaf-layout](https://github.com/cbartram/nanoleaf-layout) that has a more accurate light panel shape (trimmed corners on the triangles).
