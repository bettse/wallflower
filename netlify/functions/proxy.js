const Wreck = require('@hapi/wreck');

const {AURORA_HOST, AURORA_TOKEN, NETLIFY_DEV} = process.env;
const host = AURORA_HOST;

const headers = {
  'Content-Type': 'application/json',
};

// Docs on event and context https://www.netlify.com/docs/functions/#the-handler-method
exports.handler = async (event, context) => {
  try {
    const {httpMethod, path, body} = event;

    const apiPath = path
      .replace('TOKEN', AURORA_TOKEN)
      .replace('/.netlify/functions/proxy', '');
    const uri = `https://${host}${apiPath}`;

    const response = await Wreck.request(httpMethod, uri, {payload: body});
    const responseBody = await Wreck.read(response, {json: true});

    return {
      statusCode: response.statusCode,
      body: JSON.stringify(responseBody),
      headers,
    };
  } catch (err) {
    if (NETLIFY_DEV) {
      console.log(err)
    }
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify(err),
    };
  }
};
