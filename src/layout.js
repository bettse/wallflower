/**
 * Created by Christian Bartram on 6/20/17.
 * Github @cbartram
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';

function radians(degrees) {
  return (degrees * Math.PI) / 180;
}

class NanoleafLayout extends Component {
  trianglePath(sideLength = this.props.data.sideLength) {
    const origin = [0, 0]; // Trangle will later be translated and rotated
    let e = this.equilateral(sideLength, origin);
    let path = `M${e.topVertex[0]} ${e.topVertex[1]} L${e.leftVertex[0]} ${e.leftVertex[1]} L${e.rightVertex[0]} ${e.rightVertex[1]} L${e.topVertex[0]} ${e.topVertex[1]} Z`;
    return path;
  }

  // Based on https://gist.github.com/julienetie/92b2e87269f7f9f0bee0
  equilateral(sideLength, cen) {
    const halfSide = sideLength / 2;

    // Inner innerHypotenuse angle = 120, hyp = half side. Cos 120 * adjacent
    const innerHypotenuse = halfSide * (1 / Math.cos(radians(30)));

    // SqRt(Hyp^2 - Adj^2) pythagoras
    const innerOpposite = halfSide * (1 / Math.tan(radians(60)));

    var leftVertex = [];
    var rightVertex = [];
    var topVertex = [];

    leftVertex[0] = cen[0] - halfSide;
    leftVertex[1] = cen[1] + innerOpposite;

    rightVertex[0] = cen[0] + halfSide;
    rightVertex[1] = cen[1] + innerOpposite;

    topVertex[0] = cen[0];
    topVertex[1] = cen[1] - innerHypotenuse;

    return {topVertex, rightVertex, leftVertex};
  }

  validate() {
    const {data} = this.props;
    if (
      !data.hasOwnProperty('positionData') &&
      !data.hasOwnProperty('layoutData')
    ) {
      throw new Error(
        'Could not find property: positionData or layoutData in given prop. Ensure that your data includes a positionData or layoutData key with an array value',
      );
    }

    if (data.hasOwnProperty('layoutData')) {
      // beta (as of Mar 10, 2018) format
      // "layoutData": "9 150 150 -149 173 180 3 74 303 300 154 -74 129 240 106 -74 43 300 228 -149 0 240 35 0 259 0 26 0 173 60 142 -224 129 0 59 -224 43 300"
      // Cribbed from nanoleaves project
      const layoutData = data.layoutData.split(' ');
      data.numPanels = layoutData.shift();
      data.sideLength = layoutData.shift();
      data.positionData = [];

      while (layoutData.length) {
        data.positionData.push({
          id: layoutData.shift(),
          x: layoutData.shift(),
          y: layoutData.shift(),
          o: layoutData.shift(),
        });
      }
    }

    return data.positionData;
  }

  endCaps() {
    const {data, background} = this.props;
    const {sideLength} = data;
    const capSize = sideLength * 0.15;
    const points = this.equilateral(sideLength - capSize, [0, 0]);
    const trianglePath = this.trianglePath(capSize);

    return (
      <g>
        <g
          key="top_cap"
          transform={`translate(${points.topVertex[0]},${points.topVertex[1]})`}>
          <path
            d={trianglePath}
            fill={background}
            stroke={background}
            strokeWidth={2}
          />
        </g>
        <g
          key="left_cap"
          transform={`translate(${points.leftVertex[0]},${points.leftVertex[1]})`}>
          <path
            d={trianglePath}
            fill={background}
            stroke={background}
            strokeWidth={2}
          />
        </g>
        <g
          key="right_cap"
          transform={`translate(${points.rightVertex[0]},${points.rightVertex[1]})`}>
          <path
            d={trianglePath}
            fill={background}
            stroke={background}
            strokeWidth={2}
          />
        </g>
      </g>
    );
  }

  /**
   * Handles recalculating values and updating when the layout changes
   * @returns {Array}
   */
  update() {
    const showCenter = false; // Development
    const {onHover, onClick, onExit} = this.props;
    const {
      background,
      strokeWidth,
      strokeColor,
      color,
      showId,
      rotation,
    } = this.props;

    const panels = this.validate();
    // Sort panels so that selected panels are rendered later
    // This prevents overlaping a non-white strokeColor with white.
    panels.sort((a, b) => {
      if (a.strokeColor && !b.strokeColor) {
        return 1;
      } else if (!a.strokeColor && b.strokeColor) {
        return -1;
      }
      return 0;
    });

    const trianglePath = this.trianglePath();
    const offset = 180;
    return panels.map((value, key) => {
      return (
        <g
          key={key}
          transform={`translate(${value.x},${value.y}) rotate(${value.o +
            offset})`}>
          <path
            key={key + '_path'}
            d={trianglePath}
            strokeWidth={strokeWidth}
            onMouseOver={() => onHover(value)}
            onMouseOut={() => onExit(value)}
            onClick={() => onClick(value)}
            fill={value.color || color}
            stroke={value.strokeColor || background}
          />
          {this.endCaps()}
          {showCenter && (
            <circle key={key + '_circle'} cx={0} cy={0} r={5} fill={'pink'} />
          )}
          {showId && (
            <text
              key={key + '_text'}
              fill="#FFFFFF"
              textAnchor="middle"
              transform={`rotate(${rotation - value.o}) scale(-1, 1)`}
              onClick={e => onClick(value)}>
              {value.id}
            </text>
          )}
        </g>
      );
    });
  }

  render() {
    const {data, rotation} = this.props;
    if (data.positionData.length === 0) return null;
    let minX = 0;
    let maxX = 0;
    let minY = 0;
    let maxY = 0;

    data.positionData.forEach(panel => {
      if (panel.x > maxX) {
        maxX = panel.x;
      }
      if (panel.x < minX) {
        minX = panel.x;
      }
      if (panel.y > maxY) {
        maxY = panel.y;
      }
      if (panel.y < minY) {
        minY = panel.y;
      }
    });

    // the min/max are now based on the center of the triangles, so we want to add a sideLength so we're
    // working with the triangle bounding boxes
    maxX += data.sideLength;
    minX -= data.sideLength;
    maxY += data.sideLength;
    minY -= data.sideLength;

    const width = maxX - minX;
    const height = maxY - minY;
    const midX = minX + width / 2;
    const midY = minY + height / 2;

    // For development
    const showTrueZero = false;
    const showTransZero = false;
    const showCenter = false;

    // Translate out, scale and rotate, translate back.  Makes it 'feel' like the scale and rotation are happening around the center and not around 0,0
    const transform = `translate(${midX},${midY}) rotate(${rotation +
      180}) scale(-1,1) translate(${-midX},${-midY})`;

    /*
        //Trying to create a tighter bounding box
        const radRot = Math.abs(radians(rotation))
        const rotW = Math.sin(radRot) * height + Math.cos(radRot) * width;
        const rotH = Math.sin(radRot) * width + Math.cos(radRot) * height;

        const rotMinX = Math.sin(radRot) * minY + Math.cos(radRot) * minX;
        const rotMinY = Math.sin(radRot) * minX + Math.cos(radRot) * minY;
        */

    // Use calculated to give a tight view of the panels
    const viewBox = `${minX} ${minY} ${width} ${height}`;

    return (
      <svg
        className="nanoleaf-layout"
        viewBox={viewBox}
        preserveAspectRatio="xMidYMid meet">
        {showTrueZero && <circle cx={0} cy={0} r={5} fill="blue" />}
        <g transform={transform}>
          {this.update()}
          {showTransZero && <circle cx={0} cy={0} r={5} fill="lightblue" />}
        </g>
        {showCenter && <circle cx={midX} cy={midY} r={5} fill="red" />}
      </svg>
    );
  }
}

NanoleafLayout.propTypes = {
  data: PropTypes.object.isRequired, // should be array
  showId: PropTypes.bool,
  strokeWidth: PropTypes.number,
  opacity: PropTypes.number,
  color: PropTypes.string,
  rotation: PropTypes.number,
  onHover: PropTypes.func,
  onClick: PropTypes.func,
  onExit: PropTypes.func,
};

NanoleafLayout.defaultProps = {
  showId: false,
  opacity: 1,
  strokeWidth: 2,
  rotation: 0,
  color: '#555555',
  background: '#fff',
  strokeColor: '#eeeeee',
  onHover: data => data,
  onClick: data => data,
  onExit: data => data,
};

export default NanoleafLayout;
