import React from 'react';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import {useMqttState} from 'mqtt-react-hooks';

function Header() {
  const {connectionStatus} = useMqttState();

  return (
    <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
      <Navbar.Brand>Wallflower</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="https://gitlab.com/bettse/wallflower/">
            Sourcecode (GitLab)
          </Nav.Link>
          <Nav.Link href="#">{`Sync: ${connectionStatus}`}</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default Header;
