import React, {useState, useEffect} from 'react';

import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Toast from 'react-bootstrap/Toast';

import {CirclePicker} from 'react-color';
import tinycolor from 'tinycolor2';

import {useMqttState, useSubscription} from 'mqtt-react-hooks';

import AuroraAPI, {Animation} from './nanoleaves';

import useLocalStorage from './useLocalStorage';
import NanoleafLayout from './layout';

const DECIMAL = 10;
const ONLINE_EFFECT = 'online';
// Could interpolate hostname to allow project to be deployed by others
const SYNC_TOPIC = `wallflower/animation/sync`;

const STATUS = {
  CONNECTED: 'Connected',
  OFFLINE: 'Offline',
  RECONNECTING: 'Reconnecting',
  CLOSED: 'Closed',
};

const {NODE_ENV} = process.env;
const noop = NODE_ENV === 'production' ? () => {} : console.log;

const {location} = window;
const {hostname, protocol, port} = location;

// Use the port (ex development), or fall back to 443/80.  We have to define the port otherwise it'll default to 16021 in AuroraAPI
const apiPort = parseInt(port, DECIMAL) || (protocol === 'https:' ? 443 : 80);

const aurora = new AuroraAPI({
  host: hostname,
  protocol,
  port: apiPort,
  token: 'TOKEN',
});

function Body(props) {
  const [toasts, setToasts] = useState([]);
  const [status, setStatus] = useState('Downloading existing state...');
  const [orientation, setOrientation] = useLocalStorage('orientation', 0);
  const [animation, setAnimation] = useState(null);
  const [layout, setLayout] = useLocalStorage('layout', {positionData: []});
  const [selectedPanels, setSelectedPanels] = useState([]);
  const {client, connectionStatus} = useMqttState();
  const {message} = useSubscription([SYNC_TOPIC]);

  useEffect(() => {
    if (!message || !animation) {
      return;
    }
    const {topic, message: incoming} = message;

    if (topic === SYNC_TOPIC) {
      const {animData} = incoming;
      const serialized = animation.serialize();
      // Prevents looping on the value just published by this instance
      if (animData !== serialized.animData) {
        const newAnimation = new Animation(incoming);
        const now = Date.now();
        setToasts([
          ...toasts,
          {title: 'Update', time: now, body: 'New mqtt update'},
        ]);
        setTimeout(() => {
          setToasts(toasts.filter(toast => toast.time !== now));
        }, 3000);
        setAnimation(newAnimation);
      }
    }
  }, [message]);

  useEffect(() => {
    async function getOrientation() {
      try {
        const orientation = await aurora.orientation();
        setOrientation(orientation);
      } catch (e) {
        noop(e);
      }
    }
    getOrientation();
  }, []);

  useEffect(() => {
    const attributes = ['id', 'o', 'x', 'y', 'shapeType'];
    async function getLayout() {
      try {
        const newLayout = await aurora.layout();
        // The library wants them to be positionData
        newLayout.positionData = newLayout.panels;
        // Check out and attributes to prevent regressing the data when the localStorage cache is sufficient
        if (newLayout.count !== layout.count) {
          setLayout(newLayout);
          return;
        }
        for (var i = 0; i < layout.positionData.length; i++) {
          const panel = layout.positionData[i];
          const newPanel = newLayout.positionData[i];
          if (attributes.some(attr => panel[attr] !== newPanel[attr])) {
            setLayout(newLayout);
            return;
          }
        }
      } catch (e) {
        noop(e);
      }
    }
    getLayout();
  }, []);

  useEffect(() => {
    try {
      aurora.setEffect(ONLINE_EFFECT);
    } catch (e) {
      console.log('failed to set ONLINE_EFFECT', e);
      setStatus('Failed to set ONLINE_EFFECT');
    }
  }, []);

  useEffect(() => {
    async function getAnimation() {
      try {
        const newAnimation = await aurora.animation(ONLINE_EFFECT);
        setAnimation(newAnimation);
        setStatus('Click panels to select, then color swatch to assign.');
      } catch (e) {
        noop(e);
      }
    }
    getAnimation();
  }, []);

  useEffect(() => {
    if (animation) {
      aurora.addAnimation(animation.serialize());
    }
  }, [animation]);

  useEffect(() => {
    if (animation) {
      //Annotate panels with their colors
      const {panels} = animation;
      const {positionData} = layout;
      const newPositionData = positionData.map(panel => {
        const {frames} = panels[panel.id];
        const [color] = frames;
        panel.color = tinycolor(color).toHexString();
        return panel;
      });
      setLayout({...layout, positionData: newPositionData});
    }
  }, [animation]);

  useEffect(() => {
    const {positionData} = layout;
    const newPositionData = positionData.map(panel => {
      const {id, color} = panel;
      if (selectedPanels.includes(id)) {
        panel.strokeColor = tinycolor(color)
          .darken(50)
          .toHexString();
      } else {
        delete panel.strokeColor;
      }
      return panel;
    });
    setLayout({...layout, positionData: newPositionData});
  }, [selectedPanels]);

  const panelSelect = ({id}) => {
    if (selectedPanels.includes(id)) {
      setSelectedPanels(selectedPanels.filter(panelId => panelId !== id));
    } else {
      setSelectedPanels([...selectedPanels, id]);
    }
  };

  const colorSelect = color => {
    if (selectedPanels.length > 0) {
      selectedPanels.forEach(id => {
        const panel = animation.panels[id];
        const {frames} = panel;
        const [frame] = frames;

        animation.panels[id].frames = [{...frame, ...color.rgb}];
      });
      setSelectedPanels([]);

      const newAnimation = new Animation(animation.serialize());
      setAnimation(newAnimation);

      // Would be elegant to do using a useEffect, but triggers more often than I want
      if (connectionStatus === STATUS.CONNECTED) {
        const message = JSON.stringify(newAnimation.serialize());
        console.log('publish', newAnimation.panels);
        client.publish(SYNC_TOPIC, message);
      }
    }
  };

  return (
    <>
      <Container>
        <Row>
          <Col md={{span: 6, offset: 3}}>
            <NanoleafLayout
              data={layout}
              onClick={panelSelect}
              rotation={orientation}
            />
          </Col>
          <Col className="d-none d-md-block">
            <div
              aria-live="polite"
              aria-atomic="true"
              style={{position: 'relative', minHeight: '200px'}}>
              <div style={{position: 'absolute', top: 0, right: 0}}>
                {toasts.map((toast, index) => {
                  const {title, time, body} = toast;
                  return (
                    <Toast key={index}>
                      <Toast.Header>
                        <strong className="mr-auto">{title}</strong>
                        <small>{time}</small>
                      </Toast.Header>
                      <Toast.Body>{body}</Toast.Body>
                    </Toast>
                  );
                })}
              </div>
            </div>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col>
            <CirclePicker
              className="justify-content-center"
              width="100%"
              onChangeComplete={colorSelect}
            />
          </Col>
        </Row>
        <Row className="text-center justify-content-center pt-3">
          <Col>{status}</Col>
        </Row>
      </Container>
    </>
  );
}

export default Body;
