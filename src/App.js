import React from 'react';

import Konami from 'react-konami-code';

import {Connector} from 'mqtt-react-hooks';

import Header from './Header';
import Body from './Body';
import Footer from './Footer';
import './App.css';

const easterEgg = () => {
  console.log('Clear local storage');
  localStorage.clear();
};

function App() {
  return (
    <Connector
      brokerUrl="wss://broker.emqx.io:8084/mqtt"
      parserMethod={JSON.parse}
      options={{keepalive: 0}}>
      <Konami action={easterEgg} />
      <Header />
      <Body />
      <Footer />
    </Connector>
  );
}

export default App;
