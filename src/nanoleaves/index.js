const Aurora = require('./lib/aurora').default;
const Panel = require('./lib/panel').default;
const Animation = require('./lib/animation').default;

Aurora.Panel = Panel;
Aurora.Animation = Animation;

export {Aurora as default, Aurora, Panel, Animation};
